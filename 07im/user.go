package main

import (
	"fmt"
	"net"
	"strings"
)

type User struct {
	Name   string
	Addr   string
	C      chan string
	conn   net.Conn
	server *Server
}

// 创建一个用户 的接口
func NewUser(conn net.Conn, server *Server) *User {
	// 获取客户端地址
	userAddr := conn.RemoteAddr().String()

	user := &User{
		Name:   userAddr,
		Addr:   userAddr,
		C:      make(chan string),
		conn:   conn,
		server: server,
	}

	// 每创建一个新用户，都要单独起一个go程
	go user.ListenMessage()

	return user
}

// 监听当前User channel的方法，一旦有消息，就直接发送给对端客户端
func (that *User) ListenMessage() {
	for msg := range that.C {
		_, err := that.conn.Write([]byte(msg + "\n"))
		if err != nil {
			panic(err)
			// fmt.Println(msg, "conn.Write err:", err)
			// return
		}
	}

	err2 := that.conn.Close()
	if err2 != nil {
		panic(err2)
	}
}

// 用户上线
func (that *User) Online() {
	// 用户上线，将用户加入到onlineMap中
	that.server.mapLock.Lock()
	that.server.OnlineMap[that.Name] = that
	that.server.mapLock.Unlock()

	that.server.BroadCast(that, "已上线")
}

// 用户下线
func (that *User) Offline() {
	// 用户下线，将用户从onlineMap中删除
	that.server.mapLock.Lock()
	delete(that.server.OnlineMap, that.Name)
	that.server.mapLock.Unlock()

	// 广播当前用户下线的消息
	that.server.BroadCast(that, "已下线")
}

// 用户处理消息的业务
func (that *User) DoMessage(msg string) {
	if msg == "who" {
		// 定义消息格式：用户在客户端输入 who 时，展示当前所有在线的用户列表

		that.server.mapLock.Lock()
		for _, user := range that.server.OnlineMap {
			sendMsg := "[" + user.Addr + "]" + user.Name + ":在线...\n"
			that.SendMsg(sendMsg)
		}
		that.server.mapLock.Unlock()
	} else if len(msg) > 7 && msg[0:7] == "rename|" {
		// 定义消息格式： rename|张三  会把当前用户的用户名修改为张三

		newName := strings.Split(msg, "|")[1]

		// 判断当前用户名是否已经存在
		_, ok := that.server.OnlineMap[newName]
		if ok {
			that.SendMsg("当前名称已经存在\n")
		} else {
			that.server.mapLock.Lock()
			delete(that.server.OnlineMap, that.Name) // 删掉老的
			that.server.OnlineMap[newName] = that    // 增加新的
			that.server.mapLock.Unlock()

			that.Name = newName
			that.SendMsg("您已经更新用户名：" + that.Name + "\n")
		}

	} else if len(msg) > 4 && msg[0:3] == "to|" {
		// 定义消息格式： to|张三|你好呀 用户的私聊功能

		// 1.找到私聊对象的信息
		remoteName := strings.Split(msg, "|")[1]
		if remoteName == "" {
			that.SendMsg("消息格式不正确，请使用\"to|张三|你好啊\" 格式。\n")
			return
		}

		// 2.根据用户名，得到对方User对象
		remoteUser, ok := that.server.OnlineMap[remoteName]
		if !ok {
			that.SendMsg("该用户名不存在\n")
			return
		}

		// 3.获取消息内容，通过调用对方的sendmsg将消息发送过去
		content := strings.Split(msg, "|")[2]
		if content == "" {
			that.SendMsg("无消息内容，请重发\n")
			return
		}

		remoteUser.SendMsg(that.Name + "对您说：" + content + "\n")

	} else {
		that.server.BroadCast(that, msg)
	}
}

// 向当前用户输出一些内容（非广播）
func (that *User) SendMsg(msg string) {
	_, err := that.conn.Write([]byte(msg))
	if err != nil {
		fmt.Println("conn.Write err:", err)
	}
}
