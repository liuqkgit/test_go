package main

import (
	"fmt"
	"io"
	"net"
	"sync"
	"time"
)

type Server struct {
	Ip   string
	Port int

	// 在线用户的列表
	OnlineMap map[string]*User
	mapLock   sync.RWMutex

	// 消息广播的channel
	Message chan string
}

// 创建一个server 的接口
func NewServer(ip string, port int) *Server {
	server := &Server{
		Ip:        ip,
		Port:      port,
		OnlineMap: make(map[string]*User),
		Message:   make(chan string),
	}
	return server
}

// 启动服务器的接口
func (that *Server) Start() {
	// socket lister
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", that.Ip, that.Port))
	if err != nil {
		fmt.Println("net.listen err:", err)
		return
	}

	// close listen socket
	defer listener.Close()

	// 专门去监听 Message 管道，如果有消息，则广播到所有在线用户
	go that.ServerListenMessage()

	for {
		// accept
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("listener accept err:", err)
			continue
		}

		// do handler  使用一个单独的协程去处理
		go that.Handler(conn)
	}
}

// 处理当前链接的业务
func (that *Server) Handler(conn net.Conn) {
	fmt.Println("链接建立成功")

	user := NewUser(conn, that)

	// 处理用户上线后的事
	user.Online()

	// 监听用户是否活跃的channel
	isLive := make(chan bool)

	// 接收客户端发送的消息
	go func() {
		buf := make([]byte, 4096)
		for {
			n, err := conn.Read(buf)
			if n == 0 {
				user.Offline()
				return
			}

			if err != nil && err != io.EOF {
				fmt.Println("Conn Read err:", err)
				return
			}

			// 提取用户的消息--去掉末尾的 \n
			msg := string(buf[:n-1])

			// 将得到的消息交给user处理
			user.DoMessage(msg)

			// 用户发送任何消息，表示当前用户是活跃的
			isLive <- true
		}
	}()

	// 阻塞当前handler 防止线程退出
	for {
		select {
		case <-isLive:
			// 表示当前用户是活跃的，什么也不用做
		case <-time.After(time.Second * 300):
			// 当前已超时，强制关闭链接

			user.SendMsg("您已经被踢了")

			//销毁用的资源 - 在user中去关闭conn
			close(user.C)

			// 退出当前handler
			return // runtime.Goexit()
		}
	}

}

// 广播消息的方法
func (that *Server) BroadCast(user *User, msg string) {
	sendMsg := "[" + user.Addr + "]" + user.Name + ":" + msg

	// 将消息放入Server的Message管道中（需要有线程一直读取Message管道）
	that.Message <- sendMsg
}

// 监听Message管道，一旦有消息就发送给全部在线User。使用协程
func (that *Server) ServerListenMessage() {
	for {
		msg := <-that.Message

		// 将msg发送给全部的在线User
		that.mapLock.Lock()
		for _, cli := range that.OnlineMap {
			cli.C <- msg
		}
		that.mapLock.Unlock()
	}
}
