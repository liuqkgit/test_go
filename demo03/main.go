package main

import (
	"encoding/json"
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

// dsn规则 [username[:password]@][protocol[(address)]]/dbname[?param1=value1&...&paramN=valueN]
// param中 charset=utf8 实际为 utf8mb3 无法插入Emoji，即使该表是utf8mb4
// parseTime=True 为了处理 time.Time
// loc=Local 设置时区与本地时区保持一致
const dsn = "root:root@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"

var DB *gorm.DB // 这是一个全局的数据库链接指针！！

type t3 struct {
	Id int `sql:"id"`
	C  int `sql:"c"`
	D  int `sql:"d"`
}

type UsersInfo struct {
	ID      uint   `sql:"id" gorm:"size:10" json:"Id"`
	UserId  uint   `sql:"user_id" gorm:"" json:"user__id"`
	Address string `sql:"address" gorm:"size:255"`
	Hobby   string `sql:"hobby"`
	Age     uint   `gorm:"size:3"`
}

func init() {
	// 初始化连接数据库，这里不能用 := 而是应该用 = 赋值，否则这里的DB会声明成局部变量
	var err error
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})

	if err != nil {
		panic(err) // 中断程序，后面的代码将不会被执行
	}

	// DB.AutoMigrate(&UsersInfo{})
}

// 在这里可以自定义结构体对应的表名
func (u *UsersInfo) TableName() string {
	return "user_info_"
}

// 钩子函数
func (u *UsersInfo) BeforeDelete(tx *gorm.DB) (err error) {
	fmt.Println(u)
	return
}

func (u *UsersInfo) AfterDelete(tx *gorm.DB) (err error) {
	fmt.Println(u)
	return
}

// 声明一个带有日志功能的数据库连接，有3种方法
func initWithLogger() {
	// var mysqlLogger logger.Interface
	mysqlLogger := logger.Default.LogMode(logger.Info) // 声明一个全局的自定义的logger，后面会使用

	// 可以使用 &gorm.Config{} 来对连接做一些配置
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix:   "sys_", // 表名前缀
			SingularTable: false,  // 是否单数表名（默认会在表名后增加字母s）
		},
		// Logger: mysqlLogger, // 记录日志的方法1：在这里来声明使用自定义的logger
	})

	if err != nil {
		panic(err) // 中断程序，后面的代码将不会被执行
	}

	// 使用日志的方法2：通过session来使用自定义的logger
	db = db.Session(&gorm.Session{
		Logger: mysqlLogger,
	})

	// 使用日志的方法3：通过debug来记录日志（debug()加在哪个方法调用之前，哪次执行就启用debug）
	// db.Debug().AutoMigrate(t3{})

	// 执行数据库迁移——创建t3结构体对应的表，如果该表已经存在，则什么也不做
	db.AutoMigrate(&t3{})
}

// 插入数据
func doInsert() {
	// 插入一条数据
	user := UsersInfo{UserId: 14, Address: "北京市朝阳区xx路", Hobby: "😂"}
	fmt.Println(user, &user)
	single := DB.Create(&user) // 这里传递的是结构体的指针，因为会涉及到赋值ID等工作

	fmt.Println(user.ID)             // 新插入数据的id
	fmt.Println(single.Error)        // 插入数据的错误信息
	fmt.Println(single.RowsAffected) // 受影响的数据条数 = len(user)

	// 插入多条数据
	var users []UsersInfo // 声明一个切片，但是没有分配空间
	for i := 0; i < 5; i++ {
		users = append(users, UsersInfo{
			UserId:  uint(i),
			Address: fmt.Sprintf("北京市海淀区xx路%v号", i+1),
			Hobby:   "吃饭，睡觉",
			Age:     uint(i + 18),
		})
	}
	multi := DB.Create(&users)
	fmt.Println(users)
	fmt.Println(multi.Error, multi.RowsAffected)
}

// 查询数据
func doSelect() {
	var user, user2, user3, user4, user5, user6, user7, user8 UsersInfo
	var users, users2 []UsersInfo
	res := DB.First(&user)
	fmt.Println("查询首条记录的Error：", res.Error)
	fmt.Println("查询到的首条记录", user)

	DB.Last(&user2)
	fmt.Println("user2 =", user2)

	// Find()是查询所有数据。
	// 如果一个对象使用，只返回一条数据（应结合Limit()使用来提高性能）
	// 如果对一个切片使用，会返回全部数据
	res3 := DB.Find(&user3) // 结果为一条数据
	fmt.Println("res3.RowsAffected =", res3.RowsAffected)
	fmt.Println("user3 =", user3)
	DB.Limit(1).Find(&user4) // 结果为一条数据，但是查询性能比上面的好
	fmt.Println("user4 =", user4)

	res4 := DB.Find(&users2) // 结果为全部数据
	fmt.Println(res4.RowsAffected)

	DB.Find(&user6, []int{4, 5, 6}) // user6是一个对象
	fmt.Println("一个对象通过 Find 使用切片查询一组数据", user6)

	DB.Find(&users, []int{4, 5, 6}) // 除了传递单个值，也可以传递切片或者数组
	fmt.Println("users =")
	for _, v := range users {
		fmt.Println(v)
	}

	data, _ := json.Marshal(users)
	fmt.Println(users)
	fmt.Println(string(data))

	// 根据主键进行查询--START ，First() Last() Take() 均支持
	DB.Take(&user5, 10)
	// DB.Take(&user5, "10") // 这样也查询到了结果，但最好是传递匹配的参数类型
	fmt.Println("user5 =", user5)

	DB.Find(&user7, "id = ?", 7) // 如果主键是字符串(例如像uuid)，查询应该使用?占位符
	fmt.Println("user7 =", user7)

	user8.ID = 8 // 也可以这样通过主键来进行查询
	DB.Find(&user8)
	fmt.Println("user8 =", user8)
	// 根据主键进行查询--END

	// var user9 UsersInfo
	var user10, user11 []UsersInfo

	// 零值查询——使用map传递零值条件可以用于查询，但是使用结构体传递零值将不会被使用
	DB.Where(map[string]interface{}{"user_id": 2, "age": 0}).Find(&user10)
	// DB.Where(map[string]any{"user_id": 2, "age": 0}).Find(&user10) // 与上面代码效果一致
	// SELECT * FROM `user_info_` WHERE `age` = 0 AND `user_id` = 2
	fmt.Println(user10)

	DB.Where(&UsersInfo{UserId: 2, Age: 0}).Find(&user11)
	// SELECT * FROM `user_info_` WHERE `user_info_`.`user_id` = 2
	fmt.Println(user11)
}

// 更新数据
func doUpdate() {
	var user1, user2 UsersInfo
	DB.First(&user1)

	user1.UserId = 999
	user1.Age = 100
	DB.Save(&user1) // Save() 将更新全部字段

	// 如果保存值不包含主键，它将执行 Create，否则它将执行 Update (包含所有字段)。
	r := DB.Save(&UsersInfo{ID: 211, UserId: 3, Age: 2})
	fmt.Println(r)

	// 更新单列
	DB.Take(&user2, 5).Update("hobby", "吃饭")
	DB.Model(&UsersInfo{}).Where("user_id", 2).Update("age", 90)

	// 更新多列
	DB.Model(&UsersInfo{}).Where("age", 90).Updates(UsersInfo{
		Hobby: "睡觉",
		Age:   91,
	})
	// UPDATE `user_info_` SET `hobby`='睡觉',`age`=91 WHERE `age` = 90

	DB.Model(&UsersInfo{}).Where("user_id", 3).Updates(map[string]interface{}{
		"hobby":   "玩",
		"address": "东城区",
	})
	// UPDATE `user_info_` SET `address`='东城区',`hobby`='玩' WHERE `user_id` = 3
}

// 删除数据
func doDel() {
	// 删除最后一条数据
	// last := UsersInfo{}
	// DB.Last(&last)
	// DB.Delete(&last)

	// 批量删除数据
	DB.Where("age = ?", 21).Delete(&UsersInfo{})
}

func main() {
	// doInsert()
	doSelect()
	// doUpdate()
	// doDel()
	// fmt.Println(time.Now())
}
