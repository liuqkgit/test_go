
**参考内容**：

[Go版本管理--go.sum](https://www.cnblogs.com/failymao/p/15092623.html)

## GOPATH
老版本默认的包管理方式，1.11之前。
- 无版本控制概念
- 无法同步一致第三方版本号
- 无法指定当前项目引用的第三方版本号


## GO111MODULE

Go语言提供了GO111MODULE这个环境变量来作为 Go modules的开关，其允许设置以下参数：
- auto：只要项目包含了 `go.mod` 文件的话，启用 Go modules。
- on：启用 Go modules。推荐。
- off：禁用 Go modules。

可以通过此来设置
> go env -w GO111MODULE=on

说得直白一些，GoModule就是一个用来取代GoPath的Golang的工作空间。

之前所有的Golang文件，都需要放在GoPath目录下才能进行正确的编译和运行。而有了GoModule之后，我们就可以把Golang文件放在GoModule目录下，也可以正确地编译运行。

GoPath：我们用来存放我们从网上拉取的第三方依赖包。  
GoModule：我们用来存放我们自己的Golang项目文件，当我们自己的项目需要依赖第三方的包的时候，我们通过GoModule目录下的一个go.mod文件来引用GoPath目录src包下的第三方依赖。

## GOPROXY

这个环境变量主要用于设置Go模块代理（Go module proxy），其作用是用于使Go在后续拉取模块版本时直接通过镜像站点来快速拉取。

GOPROXY的默认值是： `https://proxy.golang.org,direct` 

后缀的 `direct` 指的是：如果在代理中没有找到合适版本的模块，会自动重定向到包原来的位置去拉取。（包名一般 github.com/xxx/包）

proxy.golang.org在国内访问不了，需要设置国内的代理。

- 阿里云：https://mirrors.aliyun.com/goproxy/
- 七牛云：https://goproxy.cn,direct

```shell
go env -w GOPROXY=https://goproxy.cn,direct
```


## GOSUMDB

用于在拉取模块时，保证拉取到的模块版本数据未经修改。如果发现不一致，也就是可能存在篡改，将会立即终止。

GOSUMDB的默认值是：`sum.golang.org` 国内也是无法访问的。但是GOSUMDB可以被Go模块代理所代理。
即：可以通过设置 `GOPROXY` 参数来同样代理校验module的功能。

也可以设置 `GOSUMDB=off` 来关闭校验，不过不建议。


## GOPRIVATE/GONOPROXY/GONOSUMDB

这三个环境变量都是用在当前项目以来了私有模块，例如公司内部使用了私有git仓库，又或者github中的私有仓库。这些都属于私有模块，都是要进行设置的，否则会拉去失败。

设置了 `GOPRIVATE` 就可以默认覆盖另外两个。

```shell
# 可以设置多个
go env -w GOPRIVATE="git.example.com,github.com/xx/xx"

# 也可以设置通配符
# 下面的设置不包括 example.com 本身
go env -w GOPRIVATE="*.example.com"
```

# 初始化一个GO项目


1. 在一个空文件夹中输入命令 `go mod init test` 即可初始化一个新项目。
2. 当前文件夹内会新增一个文件 `go.mod` 。该文件就是go用来的包管理文件。
3. 在当前文件夹内新建文件 `main.go` 内容如下：

```go
package main

import (
    "fmt"

    "github.com/aceld/zinx/znet"
)

func main() {
    s := znet.NewServer()
    fmt.Println("hello")
    fmt.Println(s)
}
```

4. `main.go` 引用了github上的一个包，需要使用 `go get` 来下载该包，具体为：

```shell
go get github.com/aceld/zinx/znet
```
5. 下载成功之后，发现项目文件夹内多了 `go.sum` 文件，并且 `go.mod` 文件发生了修改。
6. `go.sum` 文件内容解析：
   1. 每行记录由 module名、版本名、哈希组成。
   2. 具体格式： \<module\> \<version\>[/go.mod] \<hash\>
   3. go module 机制下，依赖包名+版本号描述一个具体依赖。
   4. 一般每个依赖包会包含两条记录：
   5. 第一条记录为该依赖包版本整体的哈希值（使用特定算法取的）。
   6. 第二条记录仅表示该依赖包版本中 `go.mod`文件的哈希值。
7. `go.mod` 文件新增一行：
```go
require github.com/aceld/zinx v1.1.3 // indirect

// 末尾的 // indirect 表示当前项目间接依赖 zinx 包。
// 其原因就是 zinx/znet 包内引入了 zinx 的其他包。
```

### 修改版本依赖关系

是实际开发中，可能会涉及到修改项目模块的版本依赖关系。例如：某些包在国内访问不了或者下载缓慢，需要替换一个镜像包。

具体操作如下：
> go mod edit -replace=老包名=新包名
> 使用该命令，也会同步修改 go.mod 文件的内容。
