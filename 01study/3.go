package main

import "fmt"

func main() {
	// defer语句的使用。
	// defer语句被用于预定对一个函数的调用。
	// 可以把这类被defer语句调用的函数称为延迟函数。

	// foo1()

	// defer会晚于return执行
	rr := returnAndDefer()
	fmt.Println(rr)
}
func foo1() {
	// defer语句是一个栈，后进先出，可以使用多次
	defer fmt.Println("foo1...")
	defer fmt.Println("foo1...1...")
	fmt.Println("foo1...1...1...")
}

func returnAndDefer() int {
	defer deferFunc()
	return returnFunc()
}

func deferFunc() int {
	fmt.Println("defer function called")
	return 1
}

func returnFunc() int {
	fmt.Println("return function called")
	return 2
}
