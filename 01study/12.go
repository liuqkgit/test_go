package main

import (
	"encoding/json"
	"fmt"
	"reflect"
)

// 结构体的标签可以有多个，都是key-val格式的，主要用于介绍内部属性
type User2 struct {
	Name string `json:"name66" info:"name" doc:"我的名字"`
	Age  int    `doc:"age of me" info:"666"`
}

// 结构体tag标签 及 有关结构体的JSON编解码
func main() {
	user1 := User2{"li4", 10}
	findTag(&user1)

	// 在对结构体进行编解码的时候，会查询结构体的tag标签，其中是否有一项json标签，有就用
	// 编码的过程就是 结构体 --> json
	jsonByte, err := json.Marshal(user1)
	if err != nil {
		// 如果错误不为空，则打印错误信息并停止
		fmt.Println("json marshal error", err)
		return
	}

	fmt.Printf("jsonStr=%s 或者使用 jsonStr=%v\n", jsonByte, string(jsonByte))

	// 解码的过程就是  json --> 结构体
	// 需要先声明一个对应类型的结构体，用于存储json内的数据
	user2 := User2{}
	err = json.Unmarshal(jsonByte, &user2)
	if err != nil {
		fmt.Println("unmarshar is error", err)
		return
	}

	fmt.Println(user2)
}

// 反射 获取结构体属性的tag
func findTag(str interface{}) {
	fmt.Println("======findTag=====")
	t := reflect.TypeOf(str).Elem()

	for i := 0; i < t.NumField(); i++ {
		tagjson := t.Field(i).Tag.Get("json")
		taginfo := t.Field(i).Tag.Get("info")
		tagdoc := t.Field(i).Tag.Get("doc")
		fmt.Printf("json:%v, info:%v, doc:%v\n", tagjson, taginfo, tagdoc)
	}
}
