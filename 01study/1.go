// 1 project main.go
package main

import (
	"fmt" // 导入go语言的标准库

	"study/lib1" // 这是使用的是 go module 的方式引入本地包 （当前文件与 lib1包 在同一文件夹下的情况）
	// "lib1" // 这里引用的是 GOPATH/src中的包---其实就是将当前文件夹的lib1文件夹复制过去
	// "./lib1" // 采用相对路径导包  该方法在现有使用 go module 的情况下将不能使用，此处仅作示例
	// ll "./lib1" // 采用别名导包
	// _ "./lib1" // 采用匿名方式导包
	// . "./lib1" // 采用.的方式导包
	// 如果一个包会被多个包同时导入，实际在编译运行时只会被导入一次。
)

func main() {
	// import导包、自动调用 func init(){}、函数的多返回值

	lib1.Lib1Test() // 导入本地包后，其内部函数的调用方式
	// ll.Lib1Test() // 采用别名导包，其内部函数的调用方式

	/**
	 * 对于匿名方式导包：
	 * 导包之后必须要使用该包，否则编译不过去。
	 * 但有时只是想用该包的init函数，而不需要调用内部其他函数时，就用到了匿名导包。
	 */

	// Lib1Test() // 采用.的方式导包，直接导入到了当前文件内，调用不需要带包名。
	// 但可能会产生函数名冲突等问题，还是少用

	fmt.Println("Hello World!")
	r1, r2 := foo1("hello", 111)
	fmt.Println("r1 =", r1, " r2 =", r2)

	r3, r4 := foo2("hello", 222)
	fmt.Println("r3 =", r3, " r4 =", r4)

	r5, r6 := foo3("hello", 333)
	fmt.Println("r5 =", r5, " r6 =", r6)
}

// 返回多个返回值，匿名的
func foo1(a string, b int) (int, int) {
	fmt.Println("----foo1----")
	fmt.Println("a =", a)
	fmt.Println("b =", b)

	return 11, 22
}

// 返回多个返回值，有形参名称的
func foo2(a string, b int) (r1 int, r2 int) {
	fmt.Println("----foo2----")
	fmt.Println("a =", a)
	fmt.Println("b =", b)

	// r1 r2 属于foo2的形参，初始化默认值是0，作用域为foo2函数体内
	fmt.Println("r1 =", r1)
	fmt.Println("r2 =", r2)

	r1 = 1000
	r2 = 2000

	return
}

// 返回多个返回值，有形参名称的。 如果返回值的类型相同，可以简写。
func foo3(a string, b int) (r1, r2 int) {
	fmt.Println("----foo3----")
	fmt.Println("a =", a)
	fmt.Println("b =", b)

	// r1 r2 属于foo2的形参，初始化默认值是0，作用域为foo2函数体内
	fmt.Println("r1 =", r1)
	fmt.Println("r2 =", r2)

	r1 = 1000
	r2 = 2000

	return
}
