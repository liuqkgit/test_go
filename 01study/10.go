package main

import "fmt"

type Book2 struct {
	title string
}

// interface空接口万能类型 与 类型断言机制
func main() {
	abcc(100)
	abcc("add")

	book1 := Book2{"learn go"}
	abcc(book1)
}

// 参数是一个空接口类型，可以传递任何数据（都算对是空接口的实现）
func abcc(arg interface{}) {
	fmt.Println("abcc is calling ...")
	fmt.Println(arg)

	// go 给 interface{} 空接口提供了“类型断言”机制
	value, ok := arg.(string)
	fmt.Println(value, ok)
	if !ok {
		fmt.Println("arg is not string type")
	} else {
		fmt.Println("arg is string type, value =", value)
		fmt.Printf("value type is %T\n", value)
	}
}
