package main

import "fmt"

// 声明一种新的数据类型 myint
type myint int

// 定义一个结构体
type Book struct {
	title string
	auth  string
}

// 本文是结构体相关内容
func main() {
	var a myint = 10
	fmt.Println("a =", a)
	fmt.Printf("type of a = %T\n", a)

	// 初始化一个结构体，并给它赋值
	var book1 Book
	fmt.Println(book1)
	book1.title = "php"
	book1.auth = "liming"
	fmt.Println(book1)

	// 结构体也可以这样来初始化
	book2 := Book{title: "go", auth: "lili"}
	// book2 := Book{auth: "lili", title: "go"} // 因为指定了属性名，所以与上面的初始化结果是一致的
	book3 := Book{"c", "zhangsan"} // 由于没有指定属性名，所以初始化的2个值会与结构体属性的顺序一一对应

	fmt.Println(book2, book3)

	changeBook1(book1)
	fmt.Println(book1)

	changeBook2(&book1) // 引用传递传地址
	fmt.Println(book1)
}

func changeBook1(book Book) {
	// 传递一个book的副本
	book.auth = "666"
}

func changeBook2(book *Book) {
	// 传递一个book的指针
	book.auth = "777"
}
