package main

import "fmt"

// 定义一个结构体 man
type man struct {
	name   string
	age    int
	gender string
}

// 定义一个结构体 superMan
// 这个结构体继承了结构体man的属性
type superMan struct {
	man
	level int
}

// 本文件展示一个基本的面向对象编程的方法 --- 继承
func main() {
	man1 := man{"liming", 20, "male"}
	man1.print()
	man1.walk()

	// 子类声明方式1
	// superMan1 := superMan{man{"li4", 23, "famale"}, 10}

	// 子类声明方式2 => 更容易理解一点
	var superMan1 superMan
	superMan1.name = "zhang3"
	superMan1.age = 30
	superMan1.gender = "male"
	superMan1.level = 20

	superMan1.print() // 调用父类的方法
	superMan1.walk()  // 重写父类的方法
	superMan1.fly()   // 定义自己的方法

}

func (this *man) print() {
	fmt.Println("name = ", this.name)
	fmt.Println("age = ", this.age)
	fmt.Println("gender = ", this.gender)
}

func (this *man) walk() {
	fmt.Println("man walk ...")
}

// 子类重写父类方法
func (this *superMan) walk() {
	fmt.Println("super man walk ...")
}

// 子类的新方法
func (this *superMan) fly() {
	fmt.Println("super man fly ...")
}
