package main

import "fmt"

// 如果结构体名首字母大写，表示其他包也能访问（大小写对本包内调用无影响）
type Hero struct {
	// 如果属性名首字母大写，表示对外可访问。否则只能包内访问。
	Name  string
	ad    int
	level int
}

// 本文件展示一个基本的面向对象编程的方法 --- 封装
func main() {
	hero1 := Hero{Name: "liming", ad: 100, level: 1}
	r := hero1.GetName()
	fmt.Println(r)

	// 修改当前结构体变量的属性，发现不管用。因为该方法只是修改的内部的副本的值
	hero1.SetName("zhangsan")
	fmt.Println(hero1.Name)

	// 调用一个传递指针的方法
	r2 := hero1.GetName2()
	fmt.Println(r2)

	// 调用一个传递指针的方法，此方法可以直接修改调用者的属性值
	hero1.SetName2("wangwu")
	fmt.Println(hero1.Name)
}

// 获取姓名，返回字符串。(this Hero) 表示该方法仅能让Hero的结构体调用。
// (这里的this只是形参，没其他含义)
func (this Hero) GetName() string {
	return this.Name
}

// 设置姓名，无返回值
func (that Hero) SetName(newName string) {
	// that 是调用该方法的对象的一个拷贝
	that.Name = newName
}

// // 使用指针调用
func (this *Hero) GetName2() string {
	return this.Name
}

// 使用指针的调用-- 为一个变量设置其内部的属性名
func (abcc *Hero) SetName2(newName string) {
	abcc.Name = newName
}
