package main

import (
	"fmt"
	"reflect"
)

type User struct {
	Name string
	Age  int
}

func (this *User) Call() {
	fmt.Println("user is calling")
	fmt.Printf("%v\n", this)
}

// pair 反射 reflect
func main() {

	// go变量内部组成是 type 和 value  （统称为pair）
	// type 分为    static type（静态类型：int string ...）
	//              concrete type（interface所指向的具体数据类型，系统看得见的类型）

	var a string
	// a: pair<type:string, value:"go"> 变量a内的pair情况
	a = "go"

	// 声明一个万能接口，并把变量a的值赋值给这个万能接口。
	var allType interface{}
	// allType: pair<type:string, value:"go"> 复制过来的内部的pair还是不变的
	allType = a

	// 使用万能变量的断言机制，获取变量的值。（由于不太关心是否是string，所以使用了匿名变量）
	str, _ := allType.(string)
	fmt.Println(str)

	// 声明一个user
	user1 := User{"li4", 10}
	user1.Call()

	// 反射实例
	DoFieldAndMethod(user1)
}

// 通过反射，获取变量的类型及值
func DoFieldAndMethod(input interface{}) {
	// 获取input的type
	// func TypeOf() 用来动态获取输入参数中的值的类型，如果接口为空则返回nil
	inputType := reflect.TypeOf(input)
	fmt.Println("inputType is :", inputType.Name()) // 打印type的name

	// 获取input的value
	// func ValueOf() 用来获取输入参数接口中的数据的值，如何接口为空则返回0
	inputValue := reflect.ValueOf(input)
	fmt.Println("inputValue is :", inputValue)

	// 获取input里面的字段
	// 1. 获取interface的reflect.Type，通过Type得到NumField，进行遍历
	// 2. 得到每个field，数据类型
	// 3. 通过field有一个Interface()方法得到对应的value
	for i := 0; i < inputType.NumField(); i++ {
		field := inputType.Field(i)
		value := inputValue.Field(i).Interface()

		fmt.Printf("%s: %v = %v\n", field.Name, field.Type, value)
	}

	// 获取input里面的方法
	fmt.Println("inputType.NumMethod() ==", inputType.NumMethod())
	for i := 0; i < inputType.NumMethod(); i++ {
		m := inputType.Method(i)
		fmt.Printf("%s: %v\n", m.Name, m.Type)
	}
}
