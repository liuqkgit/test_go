package main

import "fmt"

func main() {
	// 固定长度数组及动态数组（slice）

	// 固定长度的数组，数组元素会默认为0
	var myArr1 [5]int

	// 只给一部分声明值，其余元素也是会默认为0
	myArr2 := [6]int{1, 2, 3}

	// 声明一个切片，并且初始化
	myArr3 := []int{11, 22, 33}

	// 输出两个数组的类型。
	fmt.Printf("myArr1 type=%T \n", myArr1) // type=[5]int
	fmt.Printf("myArr2 type=%T \n", myArr2) // type=[6]int
	// [5]int 与 [6]int 是两个不同的数据类型。在对函数传参时需要注意。

	normalPrint(myArr1) // 普通输出

	//// 测试值拷贝传参 --- START ---
	fmt.Println("====执行完 printArr1() 之前输出 myArr1====")
	for _, value := range myArr1 {
		fmt.Println(value)
	}

	printArr1(myArr1) // 正常运行，实参与形参类型一致
	// printArr1(myArr2) // 编译出错：因为类型不符。期望 [5]int 实际传递的 [6]int

	fmt.Println("====执行完 printArr1() 之后再输出 myArr1====")
	for _, value := range myArr1 {
		fmt.Println(value)
	}
	//// 测试值拷贝传参 --- END ---

	//// 测试引用传递传参 --- START ---

	fmt.Println("====执行完 printArr2() 之前输出 myArr3====")
	for _, value := range myArr3 {
		fmt.Println(value)
	}

	// 动态数组 slice 在传参上是引用传递，而且不同元素长度的动态数组他们的形参是一致的
	// printArr2(myArr1) // 编译出错：因为类型不符。期望 []int 实际传递的 [5]int
	printArr2(myArr3) // 正常运行

	fmt.Println("====执行完 printArr2() 之后再输出 myArr3====")
	for _, value := range myArr3 {
		fmt.Println(value)
	}
	//// 测试引用传递传参 --- END   ---
}

// 这里展示3种不同的数组循环方式
func normalPrint(myArr [5]int) {
	fmt.Println("====normalPrint====")
	// 循环输出方法1
	for i := 0; i < len(myArr); i++ {
		fmt.Println(myArr[i])
	}

	// 循环输出方法2
	for index, value := range myArr {
		fmt.Println("index=", index, ",value=", value)
	}

	// 循环输出方法3，使用匿名变量
	for _, value := range myArr {
		fmt.Println("value=", value)
	}
	fmt.Println("====normalPrint====")
}

// 数组传参方式1：值拷贝（固定长度的数组在传参的时候，是最严格匹配数组类型）
func printArr1(myArr [5]int) {
	for _, value := range myArr {
		fmt.Println("value=", value)
	}
	myArr[0] = 666 // 修改其中一项的值
}

// 数组方式，传参方式1
func printArr2(myArr []int) {
	// 循环输出，使用 _匿名变量
	for _, value := range myArr {
		fmt.Println("value=", value)
	}
	myArr[0] = 666 // 修改其中一项的值
}
