package lib1

import (
	"fmt"
)

// 如果这个函数想在外部调用，方法名的首字母要大写。
func Lib1Test() {
	fmt.Println("Lib1Test...")
}

func init() {
	fmt.Println("lib1 init...")
}
