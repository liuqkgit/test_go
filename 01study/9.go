package main

import "fmt"

// 定义一个animal接口，本质是一个指针，里面声明三个方法
type animalIF interface {
	sleep()
	getColor() string // 获取动物的颜色
	GetType() string  // 获取动物的种类
}

type cat struct {
	color string
}

type dog struct {
	color string
}

// cat 结构体实现了接口的三个方法
func (this *cat) sleep() {
	fmt.Println("cat is sleep...")
}

func (this *cat) getColor() string {
	return this.color
}

func (this *cat) GetType() string {
	return "cat"
}

// dog 结构体实现了接口的三个方法。如果没有全部实现，则不能绑定与接口的关系
func (this *dog) sleep() {
	fmt.Println("dog is sleep...")
}

func (this *dog) getColor() string {
	return this.color
}

func (this *dog) GetType() string {
	return "dog"
}

// // 多态，传递一个接口
func showAnimal(animal animalIF) {
	animal.sleep()
	fmt.Println(animal.GetType())
}

// 本文件展示一个基本的面向对象编程的方法 --- 多态，使用interface
func main() {
	var animal animalIF // 接口的数据类型，父类指针
	// fmt.Println(animal) // 此处如果直接打印，结果为： <nil>
	animal = &cat{"green"}
	fmt.Printf("%v\n", animal) // 结果为： &{green}
	fmt.Printf("%T\n", animal) // 结果为： *main.cat
	animal.sleep()             // 调用的就是cat的sleep()方法，多态的现象

	co1 := animal.getColor()
	fmt.Println(co1)

	animal = &dog{"yellow"}
	animal.sleep()

	// 另外一种体现多态的方法
	cat1 := cat{"red"}
	dog1 := dog{"black"}

	showAnimal(&cat1)
	showAnimal(&dog1)
}
