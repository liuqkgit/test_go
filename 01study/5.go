package main

import "fmt"

func main() {
	// 声明slice 切片的几种方式
	// initSlice()

	// 切片的其他操作
	// useSlice()

	// 声明map 的几种方式
	// initMap()

	// map的其他操作
	useMap()
}

func initSlice() {
	// 方式1，声明一个切片，并且初始化
	slice1 := []int{1, 2, 3}
	fmt.Printf("1 type=%T val=%v \n", slice1, slice1)
	slice1[0] = 666 // 可以自由的修改切片内的值
	fmt.Printf("1 type=%T val=%v \n", slice1, slice1)

	// 方式2，声明一个切片，但是不给切片分配空间
	var slice2 []int
	// 使用nil来判断是否为空。为空时，切片的长度为0
	fmt.Println("2 len =", len(slice2))
	if slice2 == nil {
		fmt.Println("2 is empty")
	} else {
		fmt.Println("2 has val")
	}
	fmt.Printf("2 type=%T val=%v\n", slice2, slice2)
	// slice2[0] = 666 // 因为没有分配空间，所以不能直接给里面元素赋值
	slice2 = make([]int, 3) // 使用make开辟3个空间，初始化值为0
	// slice2 = make([5]int, 3) // 不能这么用，make第一个参数接收的类型有限制
	fmt.Printf("2 type=%T val=%v\n", slice2, slice2)
	slice2[0] = 666 // 分配完空间之后，就又可以自由的赋值了
	fmt.Printf("2 type=%T val=%v\n", slice2, slice2)

	// 方式3，声明一个切片，同时给切片分配空间，初始化值为0
	var slice3 []int = make([]int, 3)
	fmt.Printf("3 type=%T val=%v\n", slice3, slice3)

	// 方式4， ==> 方式3的变体
	slice4 := make([]int, 3)
	fmt.Printf("4 type=%T val=%v\n", slice4, slice4)
}

func useSlice() {
	// 使用make的第三个参数来表示切片的容量
	// 超过切片长度，但小于容量的那部分是不能直接使用的
	// 声明一个切片，长度为3，容量为5。实际打印出来效果如下：
	slice := make([]int, 3, 5)
	fmt.Printf("len=%d cap=%d val=%v\n", len(slice), cap(slice), slice)

	// slice[3] = 666 // 无法直接使用
	// 需要使用append向切片内追加元素（也可以将追加的结果赋值给一个新变量）
	slice = append(slice, 777) // 将append成功后的数组再次赋值给slice
	fmt.Println(slice)

	// 这是将append的结果赋值给一个新的变量，该语句并不会修改原先的slice
	// newSlice := append(slice, 888)

	// 使用append同时追加多个元素
	// 追加完成后，切片的长度超过了容量，容量会再扩大一倍（可能是翻倍扩，不是固定倍率）
	slice = append(slice, 888, 999, 10)
	fmt.Printf("len=%d cap=%d val=%v\n", len(slice), cap(slice), slice)

	var ss = make([]int, 5)

	// copy可以将底层数组的值依次拷贝到新数组，返回成功拷贝的元素个数
	// copy对新老数组的长度没什么限制，新数组可以比老数组短
	copy(ss, slice) // ss的长度为5，比slice短，所以只会复制其前5项
	fmt.Println(ss)

	// 这种方式声明的切片，长度与容量相等
	s := []int{1, 2, 3, 4, 5}

	// 打印切片的长度、容量、值
	fmt.Printf("len=%d cap=%d val=%v\n", len(s), cap(s), s)

	// 切片截取，左闭右开。
	// 可以通过设置下限及上限来设置截取切片[lower-bound:upper-bound]
	s1 := s[0:3] // 截取第0个元素到第3个元素，不包含第3个元素
	fmt.Println(s1)
}

func initMap() {
	// 类似PHP关联数组的样式

	// 方式1，声明一个map，并初始化（类比声明切片的方式1）
	// 最后一项的末尾也要加 ,
	test1 := map[string]string{
		"one":   "php",
		"two":   "go",
		"three": "c",
	}
	fmt.Println(test1)
	// 输出：map[one:php three:c two:go] 并未按照顺序存储，hash排序

	// 方式2，声明一个map，但是不初始化，不分配空间（类比声明切片的方式2）
	var test2 map[string]string
	// 在使用map前，需要先使用make来分配数据空间
	test2 = make(map[string]string, 5)
	test2["one"] = "php"
	test2["two"] = "go"
	test2["three"] = "c"
	fmt.Println(test2)
	fmt.Printf("2 len=%d cap=%d map=%v \n", len(test2), len(test2), test2)

	// 方式3，
	test3 := make(map[string]string)
	fmt.Println(test3)
}

func useMap() {

	map1 := map[int]string{
		1: "abc",
		2: "def",
		4: "9d3",
		5: "8你好",
	}
	fmt.Println(map1)
	// delete函数按照指定的键将元素从map中删除。无返回值。
	// 若要删除的键为nil或者在map中不存在，delete不进行任何操作。
	delete(map1, 2)
	fmt.Println(map1)

	// 打印map1的内容
	printMap(map1)
	fmt.Println(map1)

	// 声明两层map
	var map2 map[string]map[string]string
	map2 = make(map[string]map[string]string, 5)
	map2["ab"] = make(map[string]string, 2) // 在使用第二层之前也需要make
	map2["ab"]["id"] = "83j"
	map2["ab"]["name"] = "ndiem"
	map2["cd"] = make(map[string]string, 3)
	map2["cd"]["title"] = "iend"
	fmt.Println(map2)
	fmt.Printf("map2 type=%T len=%d\n", map2, len(map2))
}

// map的传参为引用传参
func printMap(newMap map[int]string) {
	fmt.Println("--------")
	for key, val := range newMap {
		fmt.Println("key =", key, "val =", val)
	}
	fmt.Println("--------")

	// 引用传参可以直接修改原来的map
	newMap[5] = "newMap edit"
}
