package main

import "fmt"

func main() {
	// 指针的使用

	/* 定义局部变量 */
	var a int = 100
	var b int = 200

	fmt.Printf("交换前，a 的值 : %d\n", a)
	fmt.Printf("交换前，b 的值 : %d\n", b)

	fmt.Printf("变量a的地址: %x\n", &a)

	// 定义一个指针类型的数据pp，该指针指向一个整型的数据。
	// 将变量a的内存地址赋值给指针pp，此时打印pp的值，与a的内存地址都是一样的。
	var pp *int = &a
	fmt.Println("指针pp指向的地址:", pp)

	/* 调用 swap() 函数
	 * &a 指向 a 指针，a 变量的地址
	 * &b 指向 b 指针，b 变量的地址
	 */
	swap(&a, &b)

	fmt.Printf("交换后，a 的值 : %d\n", a)
	fmt.Printf("交换后，b 的值 : %d\n", b)
}

func swap(x *int, y *int) {
	// 此时的形参 x=&a ，在内存中，x的内存块中记录的是a的地址。
	var temp int

	fmt.Println(x, *x, &x) // x 是指针类型的数据， *x是取该指针对应位置上的值， &x 则又是取x的内存地址

	temp = *x /* 保存 x 地址上的值 */
	*x = *y   /* 将 y 值赋给 x */
	*y = temp /* 将 temp 值赋给 y */
}
