package main

import (
	"fmt"
	"time"
)

// 有缓冲channel
// 当channel已经满，再向里面写数据，就会阻塞
// 当channel为空，从里面取数据也会阻塞
func main() {
	// 声明一个有3个空间的channel
	c := make(chan int, 3)

	fmt.Println("len(c) =", len(c), "cap(c) =", cap(c))

	go func() {
		defer fmt.Println("go程结束了")

		for i := 0; i < 4; i++ {
			c <- i
			fmt.Println("go程写数据：", i, " len(c) =", len(c), " cap(c) =", cap(c))
		}
	}()

	time.Sleep(1 * time.Second)

	for i := 0; i < 4; i++ {
		r := <-c
		fmt.Println("主进程读数据：", r)
	}

	time.Sleep(1 * time.Second)

	fmt.Println("main 结束")
}
