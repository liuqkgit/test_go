package main

import "fmt"

func main() {
	c := make(chan int)

	go func() {
		for i := 0; i < 5; i++ {
			c <- i
		}

		// close可以主动关闭一个channel
		close(c)
	}()

	// for {
	// 	// 这是if的一种简写形式，ok为true表示channel没有关闭，false表示已经关闭
	// 	if data, ok := <-c; ok {
	// 		fmt.Println(data)
	// 	} else {
	// 		// 读不到数据时，主动退出死循环
	// 		break
	// 	}
	// }

	// 上下两个循环效果一致，可以使用range来迭代不断操作channel
	for data := range c {
		fmt.Println(data)
	}

	fmt.Println("main 结束了")
}

// 使用close()可以关闭channel，channel不像文件一样需要经常去关闭。
// 只有确实没有任何数据要发送，或者想显式的结束range循环之类的，才去关闭channel。
// 关闭channel后，无法向channel再发送数据——会引发panic错误
// 关闭channel后，可以继续从channel接收数据。
// 对于nil channel，无论收发都会被阻塞。
