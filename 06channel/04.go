package main

import "fmt"

// channel 与 select
func main() {
	// 单流程下，一个go只能监控一个channel的状态，
	// select可以完成监控多个channel的状态
	// channel传参时为引用传递。

	c := make(chan int)
	quit := make(chan int)

	// sub go
	go func() {
		for i := 0; i < 10; i++ {
			fmt.Println(<-c) // 从管道中读数据之后，管道数据就少1
		}

		quit <- 0
	}()

	// main go
	x, y := 1, 1

	for {
		select {
		case c <- x:
			// 如果c可写，则该case就会进来
			x = y
			y = x + y
		case <-quit:
			// 如果quit可读，则该case就会进来
			fmt.Println("quit")
			return
			// default:
			// 如果上面都没有成功，则进入default处理流程
			// fmt.Println("nothing...")
		}
	}

	// 以上代码：
	// 主进程会不停的往 无缓冲channel c 中写内容。协程只会读10次。
	// （主进程第11次写的内容不会被读出来）

}
