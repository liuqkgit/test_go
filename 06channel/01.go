package main

import "fmt"

// 无缓冲channel
func main() {
	// 主进程与go程（协程）是互相独立运行的，执行的先后顺序也不一定
	// 于是在需要go程返回值的情况下，不能通过go程的返回值来取。
	// go程没有返回值。
	// 主进程与go程相互传递数据需要使用channel

	// 定义一个channel（无缓冲/无缓存）
	c := make(chan int)
	// c := make(chan int, 0) // 上下等价

	go func() {
		defer fmt.Println("goroutine结束")
		fmt.Println("goroutine 正在运行。。。")

		c <- 666 // 将666发送给c。
	}()

	num := <-c // 从c中接收数据，并赋值给num。

	fmt.Println("num =", num)
	fmt.Println("main goroutine 结束。。。")
}

// channel具有同步两个不同go进程数据的能力
// 在遇到使用无缓冲channel的时候。假设主进程读channel，子进程写channel。
// 主进程在执行到读channel的代码时，如果channel中没有数据，则会主进程会阻塞，直到在channel中读到了数据。
// 子进程在执行到写channel的代码时，如果把数据写到channel后，没有进程来读，子进程会阻塞，直到有进程取走了channel的数据。
