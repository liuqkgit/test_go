package v

import (
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"
)

var wg sync.WaitGroup

/*
投票V3：多线程投票（使用管道）

实现步骤：
1.开启1个协程，向 管道-numChan 内写入2~10000的全部数字。
2.开启多个协程读取 管道-numChan 内的数字，并将素数写入 管道-resChan。此处以开10个协程为例。
3.当以上的10个协程处理完 管道-numChan 内的全部数字之后，分别向 管道-targetChan 写入标识，

	表示本协程已处理完，当全部协程都处理完后，关闭 管道-resChan 。

4.开启1个协程，打印 管道-targetChan 内的全部素数。
*/
func V3(url string) {
	id, voteNum := getInput()

	numChan := make(chan int, 100) // 声明一个票数管道，用于记录投票数

	wg.Add(1)
	go addNum(numChan, voteNum) // 开启一个协程去写入数据

	var goNum int // 开启协程的数量，至多10个
	if voteNum > 10 {
		goNum = 10
	} else {
		goNum = voteNum
	}

	fmt.Printf("开启的协程数量为：%v\n", goNum)
	sucNum := 0 // 投票成功的数量

	for i := 0; i < goNum; i++ {
		wg.Add(1)
		go v3send(url, id, numChan, &sucNum)
	}

	wg.Wait()

	fmt.Printf("投票完成，尝试投票%v次，成功%v次\n窗口30秒后会自动关闭\n", voteNum, sucNum)
	time.Sleep(time.Second * 30)
}

// 从控制台获取必要投票信息（投票id，投票数量）
func getInput() (videoId int, voteNum int) {
	fmt.Print("请输入投票id（范围1~29）：")
	for {
		fmt.Scanln(&videoId)
		if videoId > 29 || videoId < 1 {
			fmt.Print("输入有误，请重新输入：")
		} else {
			break
		}
	}

	fmt.Print("请输入投票数量：")
	fmt.Scanln(&voteNum)
	return
}

// 向票数管道写入数据
func addNum(ch chan int, num int) {
	for i := 0; i < num; i++ {
		ch <- i
	}
	close(ch) // 写入完数据后直接关闭管道——关闭后的管道只能读取数据，不能写入数据
	wg.Done()
}

func v3send(url string, id int, ch chan int, sucNum *int) {
	for i := range ch {
		v := rand.Float32()*100 + 100.0
		paramStr := fmt.Sprintf("video_id=%v&uniqueCode=Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:%.2f) Gecko/20100101 Firefox/%.2f&ip=127.0.0.1", id, v, v)
		payload := strings.NewReader(paramStr)
		response, err := http.Post(url, "application/x-www-form-urlencoded", payload)
		if err != nil {
			fmt.Println("出错了:", err)
			return
		}

		body, err := io.ReadAll(response.Body)
		if err != nil {
			fmt.Println("Read body failed:", err)
			return
		}

		var resMap map[string]string
		if err := json.Unmarshal(body, &resMap); err == nil && resMap["code"] == "0" {
			*sucNum++
		} else {
			fmt.Printf("票次%v：失败\n", i)
		}

		time.Sleep(time.Millisecond * time.Duration(rand.Intn(1000)))
	}

	wg.Done() // 完成了一次协程处理
}
