package v

import (
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

// 直接给固定视频投固定票数
func V1(url string) {
	for i := 0; i < 80; i++ {
		param := fmt.Sprintf("video_id=20&uniqueCode=Mozilla/15.0 %v&ip=127.0.0.1", rand.Float64()*5)
		payload := strings.NewReader(param)

		response, err := http.Post(url, "application/x-www-form-urlencoded", payload)
		if err != nil {
			fmt.Println("出错了:", err)
			return
		}

		body, err := io.ReadAll(response.Body)
		if err != nil {
			fmt.Println("Read body failed:", err)
			return
		}

		fmt.Println(string(body))
		time.Sleep(time.Millisecond * time.Duration(rand.Intn(100)))
	}
}
