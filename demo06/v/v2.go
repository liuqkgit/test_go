package v

import (
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

// 投票V2：可变人、可变票数、较合理的UA
func V2(url string) {
	var num int // 投票数量
	var id int  // 投票id

	fmt.Print("请输入投票id（范围1~29）：")
	for {
		fmt.Scanln(&id)
		if id > 29 || id < 1 {
			fmt.Print("输入有误，请重新输入：")
		} else {
			break
		}
	}

	fmt.Print("请输入投票数量：")
	fmt.Scanln(&num)

	for i := 0; i < num; i++ {
		v := rand.Float32()*100 + 100.0
		paramStr := fmt.Sprintf("video_id=%v&uniqueCode=Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:%.1f) Gecko/20100101 Firefox/%.1f&ip=127.0.0.1", id, v, v)

		fmt.Println(paramStr)

		payload := strings.NewReader(paramStr)

		response, err := http.Post(url, "application/x-www-form-urlencoded", payload)
		if err != nil {
			fmt.Println("出错了:", err)
			return
		}

		body, err := io.ReadAll(response.Body)
		if err != nil {
			fmt.Println("Read body failed:", err)
			return
		}

		fmt.Println(string(body))
		time.Sleep(time.Millisecond * time.Duration(rand.Intn(1000)))
	}

	fmt.Printf("投票完成，尝试投票%v次\n窗口1分钟后会自动关闭", num)
	time.Sleep(time.Minute)
}
