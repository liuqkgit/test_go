package main

import (
	"fmt"
	"time"
)

// 子Goroutine
func newTask() {
	i := 0
	for {
		i++
		fmt.Printf("new Goroutine : i = %d\n", i)
		time.Sleep(1 * time.Second)
	}
}

// 主Goroutine
func main() {
	//创建一个go程 去执行newTask() 流程
	go newTask()

	// 主进程完了之后，子进程也会停掉。如果主进程都没有执行，子进程也不会执行。
	for i := 1; i < 10; i++ {
		fmt.Printf("main goroutine : i = %d\n", i)
		time.Sleep(1 * time.Second)
	}
}
