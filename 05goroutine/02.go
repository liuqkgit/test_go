package main

import (
	"fmt"
	// "runtime"
	"time"
)

func main() {
	// 用go创建一个形参为空，返回值为空的一个函数
	go func() {
		defer fmt.Println("A.defer")

		// 定义一个匿名函数，并调用
		func() {
			defer fmt.Println("B.defer")

			// 退出当前goroutine  -- 使用return只能退出当前匿名函数，父级函数不受影响
			// runtime.Goexit()
			fmt.Println("B")
		}()

		fmt.Println("A")
	}()

	// 创建一个有返回值，有参数的协程
	go func(a int, b int) bool {
		fmt.Println("a =", a, " b =", b)
		return true
	}(10, 20)

	// 写一个死循环，保持运行
	for {
		time.Sleep(1 * time.Second)
	}
}
