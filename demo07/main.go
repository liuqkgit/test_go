package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// 基本固定的接口数据返回格式
type ApiResp struct {
	Code    int                    `json:"code"`
	Message string                 `json:"message"`
	Ttl     int                    `json:"ttl"`
	Data    map[string]interface{} `json:"data"`
}

type BiLi struct {
	ID        int    `sql:"id" gorm:"size:10" json:"Id"`
	Uid       int    `sql:"uid" gorm:"" json:"user_id"`
	Name      string `sql:"name" gorm:"size:15"`
	Sign      string `sql:"sign" gorm:"size:100"`
	Level     int8   `sql:"level"`
	Video     int    `sql:"video"`
	Opus      int    `sql:"opus"`
	Following int    `sql:"following"`
	Follower  int    `sql:"follower"`
	IsActive  uint   `sql:"is_active"`
}

// dsn规则 [username[:password]@][protocol[(address)]]/dbname[?param1=value1&...&paramN=valueN]
// param中 charset=utf8 实际为 utf8mb3 无法插入Emoji，即使该表是utf8mb4
// parseTime=True 为了处理 time.Time
// loc=Local 设置时区与本地时区保持一致
const dsn = "root:root@tcp(127.0.0.1:3306)/some?charset=utf8mb4&parseTime=True&loc=Local"

var DB *gorm.DB // 这是一个全局的数据库链接指针！！

func init() {
	// 初始化连接数据库，这里不能用 := 而是应该用 = 赋值，否则这里的DB会声明成局部变量
	var err error
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})

	if err != nil {
		panic(err) // 中断程序，后面的代码将不会被执行
	}

	DB.AutoMigrate(&BiLi{})
}

// 自定义结构体对应的表名，该函数会自动调用
func (u *BiLi) TableName() string {
	return "bili"
}

// 设置公共的header头
func setHeader(req *http.Request) {
	req.Header.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36 Edg/129.0.0.0")
	req.Header.Add("cookie", "")
}

// 获取基本信息
func GetNormalInfo(uid int) (ApiResp, error) {
	// 声明返回变量
	var rr ApiResp
	var ee error
	targetUrl := fmt.Sprintf("https://api.bilibili.com/x/space/wbi/acc/info?mid=%v", uid)
	client := &http.Client{}
	req, _ := http.NewRequest("GET", targetUrl, nil)
	setHeader(req)

	resp, e1 := client.Do(req)
	if e1 != nil {
		ee = fmt.Errorf("http发送失败：%v", e1)
		return rr, ee
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	e2 := json.Unmarshal(body, &rr)
	fmt.Println(rr)

	if e2 != nil {
		ee = fmt.Errorf("http响应解码失败：%v", e2)
		return rr, ee
	}

	return rr, ee
}

// 获取投稿情况
func GetNav(uid int) (rr ApiResp, ee error) {
	targetUrl := fmt.Sprintf("https://api.bilibili.com/x/space/navnum?mid=%v", uid)

	client := &http.Client{}
	req, e1 := http.NewRequest("GET", targetUrl, nil)
	if e1 != nil {
		fmt.Println(e1)
		return
	}

	setHeader(req)
	req.Header.Add("referer", fmt.Sprintf("https://space.bilibili.com/%v", uid))

	resp, e2 := client.Do(req)
	if e2 != nil {
		ee = fmt.Errorf("http发送失败：%v", e2)
		return
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	e3 := json.Unmarshal(body, &rr)
	if e3 != nil {
		ee = fmt.Errorf("http响应解码失败：%v", e3)
	}
	return
}

// 获取粉丝及关注数
func GetFans(uid int) (ApiResp, error) {
	// 声明返回变量
	var rr ApiResp
	var ee error
	targetUrl := fmt.Sprintf("https://api.bilibili.com/x/relation/stat?vmid=%v", uid)
	client := &http.Client{}
	req, _ := http.NewRequest("GET", targetUrl, nil)
	setHeader(req)

	resp, e1 := client.Do(req)
	if e1 != nil {
		ee = fmt.Errorf("http发送失败：%v", e1)
		return rr, ee
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	e2 := json.Unmarshal(body, &rr)
	fmt.Println(rr)

	if e2 != nil {
		ee = fmt.Errorf("http响应解码失败：%v", e2)
		return rr, ee
	}

	return rr, ee
}

// 获取播放量及点赞数（如果用户不存在，该接口会提示）
func GetViews(uid int) (ApiResp, error) {
	// 声明返回变量
	var rr ApiResp
	var ee error

	targetUrl := fmt.Sprintf("https://api.bilibili.com/x/space/upstat?mid=%v", uid)
	client := &http.Client{}
	req, _ := http.NewRequest("GET", targetUrl, nil)
	setHeader(req)

	resp, e1 := client.Do(req)
	if e1 != nil {
		// ee = errors.New("http发送失败") // 构造错误值的方法1
		ee = fmt.Errorf("http发送失败：%v", e1) // 构造错误值的方法2
		return rr, ee
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	e2 := json.Unmarshal(body, &rr)
	if e2 != nil {
		ee = fmt.Errorf("http响应解码失败：%v", e2)
	}
	return rr, ee
}

func main() {
	// targetUrl := "http://httpbin.org/get"

	for i := 1; i < 2; i++ {
		rr1, ee1 := GetViews(i)
		if ee1 != nil {
			fmt.Printf("uid=%v 处理错误：%v", i, ee1)
			continue
		}

		// 声明一个存储数据的结构体变量
		info := BiLi{Uid: i}

		if rr1.Code != 0 {
			if rr1.Code == 40061 && rr1.Message == "用户不存在" {
				fmt.Println("记录该uid不存在")
				info.IsActive = 0
			}
		} else {
			// 继续获取接口数据

			res1, err1 := GetNav(i)
			if err1 == nil {
				if val, eo := res1.Data["video"].(float64); eo {
					info.Video = int(val)
				}
				if val, eo := res1.Data["opus"].(float64); eo {
					info.Opus = int(val)
				}
			}

			res2, err2 := GetFans(i)
			if err2 == nil {
				if val, eo := res2.Data["following"].(float64); eo {
					info.Following = int(val)
				}
				if val, eo := res2.Data["follower"].(float64); eo {
					info.Follower = int(val)
				}
			}
			info.IsActive = 1
		}

		fmt.Printf("%#v", info)
		fmt.Println()
		single := DB.Create(&info) // 这里传递的是结构体的指针，因为会涉及到赋值ID等工作

		fmt.Println(info.ID)             // 新插入数据的id
		fmt.Println(single.Error)        // 插入数据的错误信息
		fmt.Println(single.RowsAffected) // 受影响的数据条数 = len(user)
	}
}
