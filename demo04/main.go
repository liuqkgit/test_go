package main

import (
	"demo04/routers"
	"fmt"
	"net/http"
	"time"

	// 导入session包
	"github.com/gin-contrib/sessions"
	// 导入session存储引擎
	"github.com/gin-contrib/sessions/redis"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	// gin默认不支持session，需要借助中间件实现：(可以使用多种方式用来存储session)
	// 创建基于cookie的存储引擎， mi_yao 参数是用于加密的密钥，可以随便填写
	// store := cookie.NewStore([]byte("mi_yao"))

	// 创建基于redis的存储引擎
	store, ses_err := redis.NewStore(10, "tcp", "localhost:6379", "", []byte("secret"))
	if ses_err != nil {
		fmt.Println(ses_err)
		return
	}

	// 设置session中间件，参数mysession，指的是session的名字，也是cookie的名字
	// store是前面创建的存储引擎
	r.Use(sessions.Sessions("mysession", store))

	routers.AdminRoutersInit(r)  // 官方文档中注册分组路由的写法
	routers.ApiRoutersInit(r)    // 便于理解官方文档花括号用法的代码
	routers.CookieRoutersInit(r) // 演示cookie的使用:设置cookie、获取cookie、有效期、作用域等

	// 注册到根路由的
	r.GET("/login", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "/login is okk")
	})

	// session的使用
	r.GET("/session", func(ctx *gin.Context) {
		// 从gin.Context中获取Session对象 这个在每次使用的时候都需要先获取这个对象
		session := sessions.Default(ctx)

		// 设置Session
		session.Set("key", time.Now().Format("20060102030405"))

		// 保存session
		session.Save()

		// 获取session
		value := session.Get("key")
		v, _ := value.(string)

		ctx.JSON(200, gin.H{"message": "Session is: " + v})
	})

	r.Run(":80")
}
