package cookie

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type CookieCommon struct{}

// 获取demo设置的所有cookie
func (c CookieCommon) GetAll(ctx *gin.Context) {
	t, _ := ctx.Cookie("time")
	tmp, _ := ctx.Cookie("tmp_5s")
	a, _ := ctx.Cookie("never_0")
	cook, _ := ctx.Cookie("path_cookie")
	host, _ := ctx.Cookie("host_cookies")
	ctx.String(http.StatusOK, fmt.Sprintf("time=%s\ntmp_5s=%s\nnever_0=%s\npath_cookie=%s\nhost_cookie=%s", t, tmp, a, cook, host))
}

// 设置几个cookie：一个长时间有效的、一个短时间会过期的、
func (c CookieCommon) Set(ctx *gin.Context) {
	ctx.SetCookie("time", time.Now().Format("2006-01-02 03-04-05"), 3600, "/", "localhost", false, false)
	ctx.SetCookie("tmp_5s", "expire = 5s", 5, "/", "localhost", false, false)
	ctx.SetCookie("never_0", "never expire 0", 0, "/", "localhost", false, false)
	ctx.SetCookie("never_-1", "never expire -1", -1, "/", "localhost", false, false)
	ctx.String(http.StatusOK, "/cookie/set")
}

// 设置指定路径有效的cookie
func (c CookieCommon) SetPath(ctx *gin.Context) {
	// 需要注意的是，设置的路径需要带上前面的路由分组！！
	ctx.SetCookie("path_cookie", "path_cookie-path_cookie", 3600, "/cookie/getPath", "localhost", false, false)
	ctx.String(http.StatusOK, "cookie/setPath")
}

// 获取设置给当前路径的cookie
func (c CookieCommon) GetPath(ctx *gin.Context) {
	cook, _ := ctx.Cookie("path_cookie")
	ctx.String(http.StatusOK, "path_cookie="+cook)
}

// 设置指定二级域名有效的cookie
func (c CookieCommon) SetHost(ctx *gin.Context) {
	// 如果当前域名为 tp8.cn ，那么第五个参数设置成 tp8.cn 或者 .tp8.cn 时，对于一级域名及所有二级域名都有效。
	// 如果想要对三级域名设置cookie，例如 img.tp8.cn ，第五个参数就需要是 img.tp8.cn 或者 .img.tp8.cn
	// 		！！注意：在设置cookie时，必须是在当前可访问域名下才有效。即：当前访问的域名是 x.img.tp8.cn 时，才能设置三级域名的独享cookie
	ctx.SetCookie("host_cookies", "host_cookie-host_cookie", 3600, "/", "tp8.cn", false, false)
	ctx.String(http.StatusOK, "")
}
