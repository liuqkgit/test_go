package admin

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Common struct{}

// 定义两个公共方法

func (c Common) Success(ctx *gin.Context) {
	ctx.String(http.StatusOK, "common--success")
}

func (c Common) Fain(ctx *gin.Context) {
	ctx.JSON(http.StatusBadRequest, gin.H{
		"status": "error",
		"code":   -1,
	})
}
