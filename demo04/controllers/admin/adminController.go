package admin

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// 普通的函数声明方式
func Index(ctx *gin.Context) {
	ctx.String(http.StatusOK, "/admin/index")
}

// 普通的函数声明方式
func Article(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status": "ok",
		"code":   1,
		"data":   []string{"abc", "def"},
	})
}

// 进阶版的控制器分组方式，采用继承
type User struct {
	Common
}

func (u User) Index(ctx *gin.Context) {
	ctx.String(http.StatusOK, "user/index")
}

func (u User) Add(ctx *gin.Context) {
	u.Success(ctx)
}
