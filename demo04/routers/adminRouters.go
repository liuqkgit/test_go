package routers

import (
	"demo04/controllers/admin"
	"net/http"

	"github.com/gin-gonic/gin"
)

func AdminRoutersInit(r *gin.Engine) {
	// 官方的路由分组写法
	a := r.Group("/admin")
	{
		// 1.采用在路由中绑定处理函数的方式注册路由
		a.GET("/", admin.Index)
		a.GET("/article", admin.Article)

		// 2.采用在注册路由时直接写出处理函数
		a.GET("/article/add", func(ctx *gin.Context) {
			ctx.String(http.StatusOK, "/admin/article/add")
		})

		// 3.采用控制器继承的方式实现处理函数的绑定（控制器继承类似结构体继承）
		a.GET("/user", admin.User{}.Index)
		a.GET("/user/add", admin.User{}.Add)

		// 4.虽然这个路由也写在花括号里，但是由于调用者不同，将不能注册到admin分组，而是直接注册到了根路由
		// 所以下面路由的访问路径是 http://127.0.0.1:8080/idx
		r.GET("/idx", func(ctx *gin.Context) {
			ctx.String(http.StatusOK, "/idx")
		})
	}
}
