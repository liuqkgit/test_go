package routers

import (
	"demo04/controllers/cookie"

	"github.com/gin-gonic/gin"
)

func CookieRoutersInit(r *gin.Engine) {
	cook := r.Group("/cookie")
	{
		cook.GET("/getAll", cookie.CookieCommon{}.GetAll)
		cook.GET("/set", cookie.CookieCommon{}.Set)
		cook.GET("/setPath", cookie.CookieCommon{}.SetPath)
		cook.GET("/getPath", cookie.CookieCommon{}.GetPath)
		cook.GET("/setHost", cookie.CookieCommon{}.SetHost)
	}
}
