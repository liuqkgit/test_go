package routers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func ApiRoutersInit(r *gin.Engine) {
	// 为了便于理解官方文档中，Group()后面花括号的用法，其相当于后面罗列了分组路由的注册
	api := r.Group("/api")
	api.GET("/", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "/api/index")
	})
	api.GET("/about", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"status": "ok",
			"code":   1,
			"data":   "/api/about",
		})
	})
}
