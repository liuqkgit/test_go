package gogo

import (
	"fmt"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// 处理单个上传文件
func SaveSingle(ctx *gin.Context) {
	file, _ := ctx.FormFile("my") // 获取上传文件
	fmt.Println(file.Filename, file.Header)

	// 重命名文件，加个时间戳的前缀
	time_int := time.Now().Unix()
	time_str := strconv.FormatInt(time_int, 10)

	// 保存文件到本地
	ctx.SaveUploadedFile(file, "d://"+time_str+file.Filename)
}

func SaveMulti(ctx *gin.Context) {
	// 相同name的多个文件上传，与单文件上传到 主要区别是获取文件的方式稍有不同
	form, _ := ctx.MultipartForm()
	fmt.Printf("%#v\n", form)
	files := form.File["multi1"]

	// 重命名文件，加个时间戳的前缀
	time_int := time.Now().Unix()
	time_str := strconv.FormatInt(time_int, 10)

	for _, file := range files {
		ctx.SaveUploadedFile(file, "d://"+time_str+file.Filename)
	}
}
