package gogo

import (
	"encoding/xml"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// 它能够基于请求自动提取JSON、form表单 和 QueryString类型的数据，并把值绑定到后端指定的结构体对象中去
// 能够进行绑定的一个前提是，结构体定义了与 表单name 对应的 form标签
type User struct {
	Name string `form:"name" uri:"name"`
	Age  int    `form:"age" uri:"age"`
}

func Bind1(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "demo01/view/5.html", nil)
}

func BindUser(ctx *gin.Context) {
	// 定义结构体对象
	var user User
	// 数据绑定
	err := ctx.ShouldBind(&user)
	fmt.Println(user)
	if err != nil {
		ctx.String(http.StatusNotFound, "绑定失败")
	} else {
		ctx.String(http.StatusOK, "绑定成功")
	}
}

// json格式的数据绑定，要声明一个json的标签
type Goods struct {
	Title  string  `json:"title"`
	Amount float32 `json:"money,string"`
}

func Bind3(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "demo01/view/6.html", nil)
}

func DoBind3(ctx *gin.Context) {
	var goods Goods
	err := ctx.ShouldBind(&goods)
	fmt.Printf("%#v\n", goods)
	if err != nil {
		ctx.JSON(http.StatusOK, gin.H{
			"msg": fmt.Sprintf("绑定失败:\n%s", err),
		})
	} else {
		ctx.JSON(http.StatusOK, gin.H{
			"msg": "绑定成功",
		})
	}
}

// url参数绑定（也需要结构体中有相应的标签 uri）
func Bind4(ctx *gin.Context) {
	// 定义结构体对象
	var user User
	// 数据绑定
	err := ctx.ShouldBindUri(&user)
	fmt.Println(user)
	if err != nil {
		ctx.String(http.StatusNotFound, "绑定失败")
	} else {
		ctx.String(http.StatusOK, "绑定成功")
	}
}

type Article struct {
	Title  string `xml:"title"`
	Author string `xml:"author"`
}

// 响应xml请求
/*
使用postman发送post请求，发送原始xml数据，其中格式为：
<?xml version="1.0" encoding="UTF-8"?>
<article>
    <title>你好</title>
    <author>张三</author>
</article>
*/
func DoXml(ctx *gin.Context) {
	res, _ := ctx.GetRawData()
	art := &Article{}

	// xml.Unmarshal() 的用法同json.Unmarshal()一样
	if err := xml.Unmarshal(res, art); err == nil {
		ctx.JSON(http.StatusOK, art)
	} else {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"err": err.Error(),
		})
	}
}
