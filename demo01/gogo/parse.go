package gogo

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
)

// 这是一个Get请求的页面
func ParseGet(c *gin.Context) {
	id := c.Query("id")                 // 从url参数中解析出一个值
	page := c.DefaultQuery("page", "1") // 从url中解析出一个参数，且该参数有默认值
	user := c.QueryMap("user")          // 从url参数中解析出一个map
	tags := c.QueryArray("tags")        // 从url参数中解析出一个数组

	// 直接在浏览器页面内打印
	// c.String(200, "id=%s, page=%s, user=%s, tags=%s", id, page, user, tags)

	time_int := time.Now()

	list := [3]string{"abc", "opq", "xyz"}
	c.HTML(200, "1.html", gin.H{
		"id":       id,
		"page":     page,
		"user":     user,
		"tags":     tags,
		"list":     list,
		"time_int": time_int,
	})
}

// 这是一个处理Post请求的函数
func ParsePost(ctx *gin.Context) {
	id := ctx.PostForm("id")
	score := ctx.DefaultPostForm("score", "99")
	user := ctx.PostFormMap("user")
	hobby := ctx.PostFormArray("hobby")
	fmt.Println("id = ", id)
	fmt.Println("score =", score)
	fmt.Println("user, map:", user)
	fmt.Println("hobby, array:", hobby)
}
