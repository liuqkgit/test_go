package main

import (
	"demo01/gogo"
	"text/template"

	"github.com/gin-gonic/gin"
)

// 自定义的模板函数
func MyAdd(a, b int) int {
	return a + b
}

func main() {
	r := gin.Default()

	r.StaticFile("/favicon.ico", "./favicon.ico")

	r.SetFuncMap(template.FuncMap{
		"add2": MyAdd,
	})

	// 加载html资源
	r.LoadHTMLGlob("view/*")

	// 解析普通get参数
	r.GET("/demo1", gogo.ParseGet)

	// 解析普通post参数
	r.POST("/demo2", gogo.ParsePost)

	// 测试单个文件上传的页面
	r.GET("/form1", func(ctx *gin.Context) {
		ctx.HTML(200, "2.html", nil)
	})

	// 单个上传文件的处理页面
	r.POST("/postForm1", gogo.SaveSingle)

	// 测试多个文件上传的页面
	r.GET("/form2", func(ctx *gin.Context) {
		ctx.HTML(200, "3.html", nil)
	})

	// 处理多个文件上传
	r.POST("/postForm2", gogo.SaveMulti)

	// 数据绑定
	// 它能够基于请求自动提取JSON、form表单 和 QueryString类型的数据，并把值绑定到后端指定的结构体对象中去

	// 第一种是form表单的数据绑定形式
	r.GET("/userBind", gogo.Bind1)
	r.POST("/userBind", gogo.BindUser)

	// 第二种是解析url中的参数，称作QueryString类型的数据绑定
	// 请求地址 http://127.0.0.1:8080/userBind2?name=li4&age=18
	r.GET("/userBind2", gogo.BindUser)

	// 第三种绑定Json格式的数据
	// 页面内使用ajax异步提交JSON数据
	r.GET("/userBind3", gogo.Bind3)
	r.POST("/userBind3", gogo.DoBind3)

	// 绑定url参数
	r.GET("/userBind4/:name/:age", gogo.Bind4)
	r.GET("/userBind4/li4/18", gogo.Bind1)
	// 如果同时定义了上面的两个路由，恰巧请求地址为 http://127.0.0.1:8080/userBind4/li4/18
	// 路由会走第二个，优先精确匹配

	// 处理xml请求
	r.POST("/xml", gogo.DoXml)

	r.Run(":8080")
}
