package main

import (
	"bufio"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"regexp"
	"sync"
	"time"

	"github.com/cheggaaa/pb/v3"
)

var wg sync.WaitGroup
var bar *pb.ProgressBar // 下载进度条指针

// 下载.m3u8的第1个版本
func Type1(mainUrl string) {
	start := time.Now()
	ss := start.Format("15:04:05")
	fmt.Printf("开始时间：%#v\n", ss)

	// 寻找下载链接的前半部分
	endReg := regexp.MustCompile(`\w+\.m3u8$`)
	preUrl := endReg.ReplaceAllString(mainUrl, "")

	client := &http.Client{}
	req, _ := http.NewRequest("GET", mainUrl, nil)

	resp, e1 := client.Do(req)
	if e1 != nil {
		fmt.Printf("http发送失败：%v", e1)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	subReg1 := regexp.MustCompile(`https?://.+?\.(?:ts|jpeg)(\?.*)?`) // 匹配单个文件的下载地址.1
	var subs [][]byte
	isSubFullUrl := true
	subs = subReg1.FindAll(body, -1) // 返回全部匹配项 -1表示不限制匹配次数

	// 如果该方式没有匹配到，则换一个正则匹配
	if len(subs) == 0 {
		subReg2 := regexp.MustCompile(`[^\n]\w+\.(ts|jpeg)`) // 匹配单个文件的下载地址.2
		subs = subReg2.FindAll(body, -1)
		isSubFullUrl = false
	}

	savePath := "F:/vTmp/"
	_, e := os.Stat(savePath)
	if e != nil {
		if os.IsNotExist(e) {
			e2 := os.Mkdir(savePath, 0666)
			if e2 != nil {
				fmt.Println("创建临时文件夹失败")
				return
			}
		} else {
			fmt.Println("发生未知错误")
			return
		}
	}

	fileList := make([]string, 10)
	subNameReg := regexp.MustCompile(`([\w-]+)\.(ts|jpeg)`) // 匹配出文件名的正则
	fmt.Printf("共计需要下载%v个片段，下载中...\n", len(subs))
	bar = pb.StartNew(len(subs)) // 创建一个进度条

	for _, subUrl := range subs {
		var res [][]byte
		fileName := "" // 文件名
		if isSubFullUrl {
			res = subNameReg.FindSubmatch(subUrl)
			if res != nil {
				fileName = fmt.Sprintf("%v.%v", string(res[1]), string(res[2]))
			} else {
				fileName = fmt.Sprintf("%v%v.mp4", time.Now().Unix(), rand.Intn(1000))
			}
		} else {
			fileName = string(subUrl)
			subUrl = append([]byte(preUrl), subUrl...)
			res = nil
		}
		// fmt.Printf("%q\n", res)

		fullName := savePath + fileName       // 拼接保存文件的全文件名
		fileList = append(fileList, fullName) // 记录全文件名
		wg.Add(1)
		go download(subUrl, fullName)
	}

	wg.Wait()
	bar.Finish() // 停止进度条

	fmt.Println("下载已完成，开始合并...")
	newFile := fmt.Sprintf("%v%v.mp4", savePath, time.Now().Unix()) // 最终保存文件的名字
	combine(newFile, fileList)
	fmt.Printf("合并已完成，视频保存路径：%v，共耗时：%v", newFile, time.Since(start))
}

// 单个文件下载
func download(remoteUrl []byte, fullName string) {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", string(remoteUrl), nil)

	resp, e1 := client.Do(req)
	if e1 != nil {
		fmt.Printf("http发送失败：%v", e1)
		return
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	wg.Done()

	// 打开指定文件，并设置文件打开的模式及权限
	file, err := os.OpenFile(fullName, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Println("文件创建失败：", err)
		return
	}
	defer file.Close()
	writer := bufio.NewWriter(file)
	writer.Write(body)
	writer.Flush() // 将缓存中的数据写入文件。
	bar.Increment()
}

// 全部文件合并
func combine(newFile string, subFileList []string) error {
	file, err := os.OpenFile(newFile, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Println("文件创建失败：", err)
		return err
	}
	defer file.Close()

	writer := bufio.NewWriter(file)
	for _, v := range subFileList {
		if v == "" {
			continue
		}
		tt, e := os.ReadFile(v)
		if e != nil {
			fmt.Println("合并文件读取失败：", v, e)
			return e
		}
		_, ee := writer.Write(tt)
		if ee != nil {
			fmt.Println("合并文件写入失败：", v, e)
			return ee
		}
		os.Remove(v)
	}

	writer.Flush() // 将缓存中的数据写入文件。
	return nil
}
