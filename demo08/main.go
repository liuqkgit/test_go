package main

import (
	"fmt"
	"os"
	"regexp"
	"runtime"
	"time"
)

var (
	videoUrl   string // 文件下载地址
	savePath   string // 文件保存路径
	routineNum int    // 启动的协程数量-默认为CPU核心数
)

func init() {
	savePath = os.TempDir() // 默认系统缓存地址
	routineNum = runtime.NumCPU()
}

func main() {
	// uri := "https://v6.tlkqc.com/wjv6/202309/09/mB8fcA4D031/video/index.m3u8"
	// subUrl := "https://m3u.nikanba.live/share/0e9c7d6985b4436d25a19e33351f4c68.m3u8"
	// subUrl = "https://m3u8.heimuertv.com/play/51f2ec1cdfda48f6978bf81b8b384b45.m3u8" // 子ts带后缀的
	// subUrl = "https://c1.7bbffvip.com/video/fanrenxiuxianchuan/第123集/index.m3u8" // 子ts不带http前缀的
	fmt.Println("输入要下载的m3u8视频地址，按回车开始下载：")
	fmt.Scanln(&videoUrl)
	fmt.Println(videoUrl)
	urlReg := regexp.MustCompile(`^https?://.+`)
	isUrl := urlReg.Match([]byte(videoUrl))
	if !isUrl {
		fmt.Println("输入的视频地址有误")
		time.Sleep(time.Minute)
		return
	}

	// Type1(videoUrl)
	err := Type2(videoUrl, savePath, routineNum)
	if err != nil {
		fmt.Println("出错了")
	}
	// var cliAge = flag.Int("age", 28, "Input Your Age")
	// fmt.Println(*cliAge)

}
