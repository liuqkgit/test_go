package middleware

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

// 调用中间件的第一种方式
func MyMiddle(c *gin.Context) {
	fmt.Println("自定义中间件--1--开始")

	// 正常情况下中间件会按照顺序依次执行，当使用 Next() 方法时，会按顺序执行下一个中间件（如果有的话）
	// 执行完下一个中间件之后，再继续执行当前中间件后面的部分（当出现多个Next()时，当成栈来理解-先进后出）
	c.Next()

	// 使用Abort()会中断中间件链的调用，但是不会影响后续代码逻辑。
	// 即代码不会执行后续未执行的中间件了，但是会正常执行完当前已经执行了一部分的前置中间件。
	// c.Abort()

	// 当使用return时，会破坏代码逻辑，导致当前中间件后面代码不执行就退出了。
	// 但是前置已经执行了一半的中间件不受影响，会继续执行完成。
	// return

	fmt.Println("自定义中间件--1--结束")
}

func MyMiddle2(c *gin.Context) {
	fmt.Println("自定义中间件--2--开始")
	c.Next()
	// c.Abort()
	// return

	fmt.Println("自定义中间件--2--结束")
}

// 调用中间件的第二种方式
// gin.HandlerFunc 等价于 func(*Context) （查看源码可知，是一个函数）
// 所以MyMiddle3就必须有一个返回值，返回值是一个函数
func MyMiddle3() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("自定义中间件--3--开始")
		fmt.Println("自定义中间件--3--结束")
	}
}

func MyMiddleGroup(c *gin.Context) {
	fmt.Println("自定义-路由组中间件")
}

func MyMiddleMethod(c *gin.Context) {
	fmt.Println("自定义-局部中间件")
}
