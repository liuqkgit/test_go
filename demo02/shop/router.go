package shop

import (
	"demo02/middleware"

	"github.com/gin-gonic/gin"
)

// 这里是模块路由
func Router(s *gin.RouterGroup) {
	s.GET("/index", Index)
	// 该路由使用了局部中间件
	s.GET("/add", middleware.MyMiddleMethod, Add)
	// 可以定义多个子路由...
	// 访问地址为： http://127.0.0.1:8080/shop/index
}
