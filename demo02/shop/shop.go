package shop

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Index(c *gin.Context) {
	c.String(http.StatusOK, "shop.index")
}
func Add(c *gin.Context) {
	c.String(http.StatusOK, "shop.add")
}
