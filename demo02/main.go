package main

import (
	"demo02/middleware"
	"demo02/router"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default() // Default() 方法默认包含两个中间件

	// 加载静态资源...

	// 使用（全局）中间件 要放在加载路由之前！！！
	// 中间件可以调用多个，由此产生了“中间件链”
	r.Use(middleware.MyMiddle) // 调用中间件方式1
	r.Use(middleware.MyMiddle2)
	// r.Use(middleware.MyMiddle, middleware.MyMiddle2) // 也可以一次性注册多个中间件

	r.Use(middleware.MyMiddle3()) // 调用中间件方式2。返回一个函数，故末尾需要加()来调用

	// 加载总路由
	router.Router(r)

	r.Run(":8080")
}
