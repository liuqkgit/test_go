package item

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Index(c *gin.Context) {
	c.String(http.StatusOK, "item.index")
}
func Add(c *gin.Context) {
	c.String(http.StatusOK, "item.add")
}
