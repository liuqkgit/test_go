package item

import "github.com/gin-gonic/gin"

func Router(r *gin.RouterGroup) {
	r.GET("/index", Index)
	r.GET("/add", Add)
}
