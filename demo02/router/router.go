package router

import (
	"demo02/item"
	"demo02/middleware"
	"demo02/shop"

	"github.com/gin-gonic/gin"
)

// 总路由
func Router(r *gin.Engine) {
	i := r.Group("/item")
	s := r.Group("/shop")

	// 自定义路由组中间件，同样的要放在加载路由之前
	i.Use(middleware.MyMiddleGroup)

	// 模块路由
	item.Router(i)
	shop.Router(s)
}
