这个文件夹是为了测试分组路由及中间件

## 中间件

+ 使用 `*gin.Engine.Use()` 可以调用中间件。支持多次调用，从而形成“中间件链”。
+ 使用 `Context.Next()` 可以暂停当前中间件而去执行中间件链的下一个中间件，执行完后再继续执行当前。
+ 使用 `Context.Abort()` 可以阻止中间件链继续执行下一个中间件，但是不会影响本中间件后续的业务代码。
+ 使用 `return` 可以破坏中间件链，让当前中间件不能全部执行完。


中间件按照使用范围可以分为三种：
1. 全局中间件，在main.go中定义
2. 路由组中间件，在加载模块路由（/路由组）之前使用
3. 局部中间件，在单条路由中使用